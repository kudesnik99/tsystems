package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int step = 2;
        int number = 1;
        int index = 0;

        if (inputNumbers == null || inputNumbers.size() <= 1) throw new CannotBuildPyramidException();

        try {
            inputNumbers.sort(null);

            while (number < inputNumbers.size()) {
                number += step++;
            }

            if (number != inputNumbers.size()) throw new CannotBuildPyramidException();

            int rows = step - 1;
            int cols = rows + (rows - 1);

            int[][] result = new int[rows][cols];

            for (int row = 0; row < rows; row++) {
                for (int col = (cols - 1) / 2 - row; col <= (cols - 1) / 2 + row; col += 2) {
                    result[row][col] = inputNumbers.get(index++);
                }
            }

            return result;

        } catch (OutOfMemoryError | NullPointerException e) {
            throw new CannotBuildPyramidException();
        }
    }
}
