package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.util.ArrayDeque;
import java.util.Locale;
import java.util.Scanner;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        String polandRecord = convert(statement);
        return (polandRecord == null) ? null : calculate(polandRecord);
    }

    // Проверка строки на допустимость и перевод в обратную польскую запись
    private static String convert(String str) {
        if (str == null) return null;
        // На входе разрешены только числа (\d+), знаки операций (*/+-) и скобки,
        // причём первый символ - только цифра (^\d).
        if (!str.matches("^\\d[\\d+*/\\-(). ]*")) return null;

        String currentLiteral;
        StringBuilder outputStr = new StringBuilder();
        ArrayDeque<String> stack = new ArrayDeque<>();

        // Принудительно добавляем пробелы вокруг скобок и знаков операций для scanner.next()
        Scanner scanner = new Scanner(str.replaceAll("[()+\\-*/]", " $0 "));

        while (scanner.hasNext()) {
            currentLiteral = scanner.next();
            if (currentLiteral.matches("\\d+.?\\d*")) {  // Число
                outputStr.append(currentLiteral).append(' ');
            } else if (currentLiteral.matches("\\(")) {    // Открывающая скобка
                stack.add(currentLiteral);
            } else if (currentLiteral.matches("\\)")) {    // Закрывающая скобка
                while (!stack.peekLast().matches("\\(")) {
                    outputStr.append(stack.pollLast()).append(' ');
                }
                stack.removeLast();
            } else {
                if (!stack.isEmpty()) {
                    if ((stack.peekLast().matches("[*/]") && currentLiteral.matches("[+\\-]")) ||
                            (stack.peekLast().matches("-") && currentLiteral.matches("\\+")) ||
                            (stack.peekLast().matches("/") && currentLiteral.matches("\\*")))
                        outputStr.append(stack.pollLast()).append(' ');
                }
                stack.add(currentLiteral);
            }
        }

        while (!stack.isEmpty()) {
            outputStr.append(stack.pollLast()).append(' ');
        }
        return outputStr.toString();
    }

    private static String calculate(String str) {
        ArrayDeque<Expr> stack = new ArrayDeque<>();
        String currentLiteral;

        Scanner scanner = new Scanner(str);
        scanner.useLocale(Locale.US); // Чтобы nextFloat понимал точку, а не запятую

        while (scanner.hasNext()) {
            if (scanner.hasNextFloat()) stack.add(new Num(scanner.nextFloat()));
            else {
                currentLiteral = scanner.next();
                switch (currentLiteral) {
                    case "*":
                        stack.add(new Mult(stack.pollLast(), stack.pollLast()));
                        break;
                    case "/":
                        stack.add(new Div(stack.pollLast(), stack.pollLast()));
                        break;
                    case "+":
                        stack.add(new Sum(stack.pollLast(), stack.pollLast()));
                        break;
                    case "-":
                        stack.add(new Diff(stack.pollLast(), stack.pollLast()));
                        break;
                }
            }
        }

        float floatResult = 0;
        String result;
        try {
            Expr stackTop = stack.pollLast();
            if (stackTop != null)
                floatResult = round(stackTop.eval());

            if (floatResult * 10000 % 10000 == 0)
                result = String.format("%.0f", floatResult);
            else if (floatResult * 10000 % 1000 == 0)
                result = String.format("%.1f", floatResult).replace(",", ".");
            else if (floatResult * 10000 % 100 == 0)
                result = String.format("%.2f", floatResult).replace(",", ".");
            else if (floatResult * 10000 % 10 == 0)
                result = String.format("%.3f", floatResult).replace(",", ".");
            else
                result = String.format("%.4f", floatResult).replace(",", ".");
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    private static float round(float value) {
        BigDecimal bd = new BigDecimal(Float.toString(value));
        return bd.setScale(4, BigDecimal.ROUND_HALF_UP).floatValue();
    }
}

interface Expr {
    default float eval() {
        return 0;
    }
}

abstract class Action implements Expr {
    Expr left;
    Expr right;

    Action(Expr left, Expr right) {
        // Первым идёт правый операнд, т.к. при вычислении, значения достаются из стека
        this.left = right;
        this.right = left;
    }
}

class Num implements Expr {
    private float value;

    Num(float value) {
        this.value = value;
    }

    @Override
    public float eval() {
        return value;
    }
}

class Mult extends Action {
    Mult(Expr left, Expr right) {
        super(left, right);
    }

    @Override
    public float eval() {
        return left.eval() * right.eval();
    }
}

class Div extends Action {
    Div(Expr left, Expr right) {
        super(left, right);
    }

    @Override
    public float eval() {
        return left.eval() / right.eval();
    }
}

class Sum extends Action {
    Sum(Expr left, Expr right) {
        super(left, right);
    }

    @Override
    public float eval() {
        return left.eval() + right.eval();
    }
}

class Diff extends Action {
    Diff(Expr left, Expr right) {
        super(left, right);
    }

    @Override
    public float eval() {
        return left.eval() - right.eval();
    }
}
