package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if ((x == null) || (y == null)) throw new IllegalArgumentException("Котик не инициализирован");
        if (y.size() < x.size()) return false;

        int startJ = 0, i, j;

        for (i = 0; i < x.size(); i++) {
            for (j = startJ; j < y.size(); j++) {
                startJ++;
                if (x.get(i) == y.get(j)) break;
            }
            if (startJ >= y.size()) return false;
        }
        return true;
    }
}
